package com.h3c

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

object Main {


  def main(args: Array[String]): Unit = {

    case class Test(name: String, value: Int, ts: Long, var first: Long, var second: Long)

    val sc = new SparkConf()
    sc.setMaster("local[2]").setAppName("wes")
    val ss = new StreamingContext(sc, Seconds(10))
    val lines = ss.socketTextStream("localhost", 9999)

    val received = lines.window(Seconds(10), Seconds(1))
    //    received.context.sparkContext.startTime
    received.mapPartitions(p => {
      p.flatMap(i => {
        try {
          // 解析数据
          List(Test("wes", 12, 0L, 0L, 0L))
        } catch {
          case _ => Nil
        }
      })
    }, false)
      .foreachRDD((rdd, batchTime) => {

        rdd.filter(item => {

          // 过滤满足条件的样本（时间段符合的）
          item.ts < batchTime.milliseconds + 10
        }).mapPartitions(p => {
          p.map(i => {
            if (i.ts == batchTime.milliseconds + 1) {
              i.first = i.value
            } else {
              i.second = i.value
            }
            i
          })
        }).foreachPartition(p => {
          val result = p.reduce((t1, t2) => {
            t1.first = t1.first + t2.first
            t1.second = t1.second + t2.second
            t1
          })
          // 计算最终的比率
          val rate = result.first / result.second
        })
      })
    ss.start()
    ss.awaitTermination()
  }
}
